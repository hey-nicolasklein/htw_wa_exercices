\contentsline {section}{\numberline {1}Vorwort}{2}{section.1}
\contentsline {section}{\numberline {2}Die Welt in unseren Taschen}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Das Anti-Social Media}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Die gr\IeC {\"o}\IeC {\ss }te Erfindung des 21. Jahrhunderts}{2}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Was Mark Zuckerberg eigentlich erfunden hat}{2}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Social Media in einer ganz trockenen Perspektive}{3}{subsection.2.4}
\contentsline {subsubsection}{\numberline {2.4.1}Die beliebtesten Tiere Deutschlands }{3}{subsubsection.2.4.1}
\contentsline {subsubsection}{\numberline {2.4.2}Die zweite Tabelle}{3}{subsubsection.2.4.2}
\contentsline {section}{\numberline {3}Machinelearning and Social-Media}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}K-Means}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Decision Trees}{4}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}HAC}{5}{subsection.3.3}
\contentsline {section}{\numberline {4}Eigene Kunstwerke}{5}{section.4}
\contentsline {section}{\numberline {5}Mathe macht Spa\IeC {\ss }}{6}{section.5}
\contentsline {subsection}{\numberline {5.1}Formeln in Align Umgebung}{6}{subsection.5.1}
\contentsline {subsubsection}{\numberline {5.1.1}Erste Gleichung}{6}{subsubsection.5.1.1}
\contentsline {subsubsection}{\numberline {5.1.2}Zweite Gleichung}{7}{subsubsection.5.1.2}
\contentsline {subsubsection}{\numberline {5.1.3}Abgesetzte Formeln}{7}{subsubsection.5.1.3}
\contentsline {subsubsection}{\numberline {5.1.4}Es wird wieder Zeit f\IeC {\"u}r einen Text}{7}{subsubsection.5.1.4}
\contentsline {section}{\numberline {6}Zitieren ist auch ganz sch\IeC {\"o}n}{7}{section.6}
\contentsline {subsection}{\numberline {6.1}Aufz\IeC {\"a}hlungen sind auch super}{8}{subsection.6.1}
\contentsline {subsubsection}{\numberline {6.1.1}Gute Mac Anwendungen}{8}{subsubsection.6.1.1}
\contentsline {subsubsection}{\numberline {6.1.2}Sinnvolle Investition von Zeit}{9}{subsubsection.6.1.2}
\contentsline {subsection}{\numberline {6.2}Nun gehts an den Code}{9}{subsection.6.2}
\contentsline {subsubsection}{\numberline {6.2.1}Python in Latex}{9}{subsubsection.6.2.1}
\contentsline {subsubsection}{\numberline {6.2.2}Algorythmus}{10}{subsubsection.6.2.2}
\contentsline {section}{\numberline {7}Schlusswort}{11}{section.7}
