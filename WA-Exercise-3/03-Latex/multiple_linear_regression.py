import pandas as pd
from pandas import DataFrame
import numpy as np
from sklearn import linear_model
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error

# read dataset into frame
happiness_data = pd.read_csv('input_data/happiness-report-2016.csv')

# extract target feature
happiness_score: DataFrame = happiness_data.iloc[:, [3]].values

# get different features for scatter plot
features: DataFrame = happiness_data.iloc[:, [6, 7, 8, 9, 10]]

# extract region data and one hot encode to use as numerical value
region: DataFrame = happiness_data.iloc[:, [1]]
region_one_hot_encoding: DataFrame = pd.get_dummies(region)
# combine our original features with the one hot encoded regions
features = pd.concat([features, region_one_hot_encoding], axis=1)

# get a linear regression object
regression = linear_model.LinearRegression()

# split the data into 20% test --> 80% train -- rename so the names make it clearer what is what
x_train, x_test, y_train, y_test = train_test_split(features, happiness_score, test_size=0.2)

# fit the line to our data respecting the happiness score
regression.fit(x_train, y_train)
predictions = regression.predict(x_test)

# calculate parameters
w_1 = regression.coef_
w_0 = regression.intercept_
corr_coefficient = np.corrcoef(predictions.reshape(1, -1), y_test.reshape(1, -1))
# mse = sum((predictions.reshape(1, -1) - y_test.reshape(1, -1)) ** 2) / len(y_test)
mse = mean_squared_error(y_test, predictions)

print(f'Correlation: {round(corr_coefficient[0, 1], 3)}\n'
      f'MSE: {round(mse, 3)}\n'
      f'w_0 = {round(w_0[0], 3)}\n')
# print optimal coefficients
for index, w in enumerate(w_1[0]):
    print(f'w_{index + 1}: {round(w, 3)}')
